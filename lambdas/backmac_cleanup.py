#!/usr/bin/env python3

import os
import pprint
import time

import boto3

import backmac_utils


def get_exports():
    print('Getting Exports list')
    cfn = boto3.client('cloudformation', region_name=os.environ['AWS_REGION'])
    exports_dict = cfn.list_exports()
    return exports_dict


def ssm_wait_response(backmac_instance, cmd):
    print('Issuing ssm command to {} : {}'.format(backmac_instance, cmd))
    ssm = boto3.client('ssm', region_name=os.environ['AWS_REGION'])
    ssm_command = ssm.send_command(
        InstanceIds=[backmac_instance],
        DocumentName='AWS-RunShellScript',
        Parameters={'commands': [cmd]},
    )
    print("command id is ", ssm_command['Command']['CommandId'])
    status = 'Pending'
    while status == 'Pending' or status == 'InProgress':
        time.sleep(3)
        list_command = ssm.list_commands(CommandId=ssm_command['Command']['CommandId'])
        status = list_command[u'Commands'][0][u'Status']
    result = ssm.get_command_invocation(
        CommandId=ssm_command['Command']['CommandId'], InstanceId=backmac_instance
    )
    return (result[u'ResponseCode'], result[u'StandardOutputContent'])


def get_backmac_info(backmac_instance):
    # interrogating backmac instance for resources needing cleanup
    print('looking for things to cleanup on {}'.format(backmac_instance))
    ec2 = boto3.client('ec2', region_name=os.environ['AWS_REGION'])
    backmac_description = ec2.describe_instances(InstanceIds=[backmac_instance])
    backmac_blockdevices = backmac_description[u'Reservations'][0][u'Instances'][0][
        u'BlockDeviceMappings'
    ]
    backmac_device_details = list(
        filter(lambda device: device['DeviceName'] == '/dev/xvdz', backmac_blockdevices)
    )
    try:
        backup_volume = backmac_device_details[0][u'Ebs'][u'VolumeId']
    except Exception:
        print('nothing mounted to backmac on /dev/xvdz')
        backup_volume = ''
    return backup_volume


def detach_volume(backmac_instance, backup_volume):
    # backup device is always attached as /dev/xvdf so lets detach that on cleanup
    print('Detaching volume /dev/xvzd on {}'.format(backmac_instance))
    ec2 = boto3.client('ec2', region_name=os.environ['AWS_REGION'])
    try:
        ec2.detach_volume(
            Device='/dev/xvdz',
            Force=True,
            InstanceId=backmac_instance,
            VolumeId=backup_volume,
        )
    except Exception as e:
        print('type is:', e.__class__.__name__)
        pprint.pprint(e)
        return 'failed'


def remove_security_groups(backmac_instance, backmac_sg):
    print(
        'Removing non-backmac security groups from backmac instance {}'.format(
            backmac_instance
        )
    )
    # add the service security group to backmac node
    ec2 = boto3.client('ec2', region_name=os.environ['AWS_REGION'])
    ec2.modify_instance_attribute(InstanceId=backmac_instance, Groups=backmac_sg)


def terminate_ec2(instance):
    ec2 = boto3.client('ec2', region_name=os.environ['AWS_REGION'])
    response = ec2.terminate_instances(InstanceIds=[instance])
    return response


def lambda_handler(event, context):
    backmac_utils.store_lambda_env(event, context)

    try:
        backmac_instance = event['backmac_instance']
    except KeyError:
        # there's no backmac_instance; nothing past this point matters
        print('No backmac_instance found in event; nothing to clean')
        return event

    try:
        sg_list = [event['security_group_id']]
    except KeyError:
        exports_dict = get_exports()
        sg_dict = list(
            filter(
                lambda resource: resource['Name'] == 'BackmacSG',
                exports_dict[u'Exports'],
            )
        )
        event['security_group_id'] = sg_dict[0][u'Value']
        sg_list = [event['security_group_id']]

    # unmount /backup
    cmd = 'umount -f /backup'
    cmd_response = ssm_wait_response(backmac_instance, cmd)
    print("unmounting /backup completed with returncode ", cmd_response[0])

    # unmount /media/atl
    cmd = 'umount -f /media/atl'
    cmd_response = ssm_wait_response(backmac_instance, cmd)
    print("unmounting /media/atl completed with returncode ", cmd_response[0])

    # detach EBS volume and security group(s)
    backup_volume = get_backmac_info(backmac_instance)
    if not backup_volume == '':
        detach_volume(backmac_instance, backup_volume)
    if not len(sg_list) == 0:
        remove_security_groups(backmac_instance, sg_list)

    # terminate the instance
    print(terminate_ec2(backmac_instance))
    return event


if __name__ == "__main__":
    '''
        simulate lambda env vars
        '''
    os.environ['AWS_LAMBDA_LOG_GROUP_NAME'] = "/aws/lambda/awsbackup_lambda"
    os.environ[
        'AWS_LAMBDA_LOG_STREAM_NAME'
    ] = "2999/00/11/[$LATEST]000 dummy stream name 000"
    os.environ['AWS_REGION'] = "us-east-1"

    event = {"dr_region": "us-east-1", "stack_name": "jira-stack"}

    context = ''
    result = lambda_handler(event, context)
    pprint.pprint(result)
