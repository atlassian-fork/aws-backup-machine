#!/usr/bin/env python3

import os
import pprint

import backmac_notify as notify


def lambda_handler(event, context):
    print(notify.send("success", 'All stacks added to execution queue', event))
    return event


if __name__ == "__main__":
    '''
        simulate lambda env vars
        '''
    os.environ['AWS_LAMBDA_LOG_GROUP_NAME'] = "/aws/lambda/awsbackup_lambda"
    os.environ[
        'AWS_LAMBDA_LOG_STREAM_NAME'
    ] = "2999/00/11/[$LATEST]000 dummy stream name 000"
    os.environ['AWS_REGION'] = "us-east-2"

    stack_name = 'jira-stack'
    event = {
        "backup_machine": "some_backup_machine",
        "dr_region": "us-east-1",
        "ebs_backup_vol": "vol-00000000000000000",
        "ebs_snap": "snap-00000000000000000",
        "rds_dr_snap": "dr-jira-stack-snap-000000000000",
        "rds_snap": "jira-stack-snap-000000000000",
        "rds_snap_arn": "arn:aws:rds:us-west-2:000000000000:snapshot:jira-stack-snap-000000000000",
    }
    context = ''
    result = lambda_handler(event, context)
    pprint.pprint(result)
